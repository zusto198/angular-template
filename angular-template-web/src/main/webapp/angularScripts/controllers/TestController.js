var testController = angular.module('TestModule', []);

testController.controller('TestController', ["TestService", "$scope", "$location", function(TestService, $scope, $location){

    $scope.data = TestService.query();

}]);