var services = angular.module('UserModule', ['ngResource']);

services.factory('UserService', ["$resource", function ($resource) {

    var baseUrl = 'rest/gui/user';
    return $resource(baseUrl, {},
        {
            authenticate: {
                url: baseUrl + '/authenticate',
                method: 'POST',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
                }
            },
            logout: {
                url: baseUrl + '/:action',
                method: 'GET',
                params: {'action': 'logout'}
            }
        }
    );
}]);


var loginModule = angular.module('LoginModule', ['ngCookies']);

loginModule.controller('LoginController', ["$scope", "$rootScope", "$location", "$cookieStore", "UserService", "Notification", function ($scope, $rootScope, $location, $cookieStore, UserService, Notification) {

    var vm = this;

    vm.login = function () {
        UserService.authenticate({username: vm.username, password: vm.password}, function (authenticationResult) {
            var authToken = authenticationResult.token;
            $rootScope.authToken = authToken;
            if (vm.rememberMe) {
                document.cookie = 'authToken=' + authToken + '; expires=' + new Date(new Date().getTime() + 7 * 24 * 3600000);
            }
            UserService.get(function (user) {
                $rootScope.user = user;
                $location.path("/");
            });
        }, function () {
            Notification('Nieprawid\u0142owy login lub has\u0142o.');
        });
    };
}]);



