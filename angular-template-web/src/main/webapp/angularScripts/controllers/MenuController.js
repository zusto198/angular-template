var menuController = angular.module('MenuController', ['ngCookies']);

menuController.controller('MenuController', ["$rootScope", "$scope", "$location", "$cookieStore", "UserService", "Notification",
    function($rootScope, $scope, $location){
    $scope.goTo = function(link){
        $location.path(link);
    };

}]);

