var mainModule = angular.module('mainModule', [
    'ngRoute',
    'ngTable',
    'datePicker',
    'ui.bootstrap',
    'ngMaterial',
    'ngSanitize',
    'ui.select',
    'ui-notification',
    'CommonDirectives',
    'MenuController',
    'LoginModule',
    'UserModule',
    'TestModule',
    'UserService',
    'TestService'
]);

mainModule.config(["$routeProvider", "$httpProvider", function($routeProvider, $httpProvider){

	$routeProvider.when('/login', {
		templateUrl: 'views/login.html',
		controller: 'LoginController'
	});

    $routeProvider.when('/', {
        templateUrl: 'views/mainpage.html'
    }).when('/test', {
        templateUrl: 'views/test.html',
        controller: 'TestController'
    }).when('/404', {
        templateUrl: 'views/404.html'
    }).otherwise({
        redirectTo: '/404'
    });

	/* Register error provider that shows message on failed requests or redirects to login page on
	 * unauthenticated requests */
	$httpProvider.interceptors.push(["$q", "$rootScope", "$location", function ($q, $rootScope, $location, Notification) {
        return {
            'responseError': function(rejection) {
                var status = rejection.status;
                var config = rejection.config;
                var method = config.method;
                var url = config.url;

                if (status == 401) {
                    Notification('nie masz uprawnien');
                    $location.path( "/login" );
                } else {
                    $rootScope.error = method + " on " + url + " failed with status " + status;
                }

                return $q.reject(rejection);
            }
        };

	}]);


}]).run(["$rootScope", "$location", "$cookieStore", "UserService", function($rootScope, $location, $cookieStore, UserService) {

    UserService.get(function(user) {
        $rootScope.user = user;
    });

    $rootScope.hasRole = function(role) {

        if ($rootScope.user === undefined) {
            return false;
        }

        if ($rootScope.user.roles[role] === undefined) {
            return false;
        }

        return $rootScope.user.roles[role];
    };

    $rootScope.logged = function() {
        return $rootScope.user;
    };

    $rootScope.logout = function() {
        UserService.logout(function(){
            delete $rootScope.user;
            delete $rootScope.authToken;
            $cookieStore.remove('authToken');
            $location.path("/login");
        });
    };
}]);