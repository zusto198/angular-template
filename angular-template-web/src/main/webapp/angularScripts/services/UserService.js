var services = angular.module('UserService', ['ngResource']);

services.factory('UserService', ["$resource", function($resource) {

    var baseUrl = 'rest/gui/user';
    return $resource(baseUrl, {},
        {
            authenticate: {
                url: baseUrl + '/authenticate',
                method: 'POST'
            },
            logout: {
                url: baseUrl + '/:action',
                method: 'GET',
                params: {'action' : 'logout'}
            }
        }
    );
}]);
