var testService = angular.module('TestService', ['ngResource']);

testService.factory('TestService', ["$resource", function($resource) {

    var baseUrl = './rest/gui/test/';

     return $resource(baseUrl, {}, {
            query: { method:'GET', isArray:true }
     });
 }]);
