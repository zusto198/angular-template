var commonModule = angular.module('CommonDirectives', []);

commonModule.directive('header',function(){

    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'views/header.html',
        controller: 'LoginController',
        controllerAs: 'vm'
    };
});
