package pl.proskar.angular.template.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TokenUtils {

	private static Logger log = LoggerFactory.getLogger(TokenUtils.class);
	public static final String MAGIC_KEY = "ahm6aiTh";
	public static final String SEPERATOR = "_";

	public static String createToken(UserDetails userDetails) {
		/* Expires in one hour */
		long expires = System.currentTimeMillis() + 7*24*3600000l;

		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(userDetails.getUsername());
		tokenBuilder.append(SEPERATOR);
		tokenBuilder.append(expires);
		tokenBuilder.append(SEPERATOR);
		tokenBuilder.append(TokenUtils.computeSignature(userDetails, expires));

		return tokenBuilder.toString();
	}

	public static String computeSignature(UserDetails userDetails, long expires) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(userDetails.getUsername());
		signatureBuilder.append(SEPERATOR);
		signatureBuilder.append(expires);
		signatureBuilder.append(SEPERATOR);
		signatureBuilder.append(userDetails.getPassword());
		signatureBuilder.append(SEPERATOR);
		signatureBuilder.append(TokenUtils.MAGIC_KEY);

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}

		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}

	public static String getUserNameFromToken(String authToken) {
		if (null == authToken) {
			return null;
		}

		String[] parts = authToken.split(SEPERATOR);
		return parts[0];
	}

	public static boolean validateToken(String authToken, UserDetails userDetails) {
		try {
			String[] parts = authToken.split(SEPERATOR);
			long expires = Long.parseLong(parts[1]);
			String signature = parts[2];

			if (expires < System.currentTimeMillis()) {
				return false;
			}
			return signature.equals(TokenUtils.computeSignature(userDetails, expires));
		}
		catch (Exception e) {
			log.error("", e);
			return false;
		}
	}
}
