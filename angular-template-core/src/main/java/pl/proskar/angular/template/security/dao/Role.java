package pl.proskar.angular.template.security.dao;

import javax.persistence.*;

@Table(name = "user_role", schema = "public")
@Entity
public class Role {


	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, insertable = true, updatable = true)
	@Id
	private Integer id;

	@Column(unique = true)
	private String name;

	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false, referencedColumnName = "id", insertable = true, updatable = true)
	private User user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
