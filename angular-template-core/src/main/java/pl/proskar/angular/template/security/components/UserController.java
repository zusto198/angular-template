package pl.proskar.angular.template.security.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.proskar.angular.template.dto.AuthDTO;
import pl.proskar.angular.template.security.ErrorResponse;
import pl.proskar.angular.template.security.TokenTransfer;
import pl.proskar.angular.template.security.TokenUtils;
import pl.proskar.angular.template.security.UserTransfer;
import pl.proskar.angular.template.security.dao.NotAuthorizedException;
import pl.proskar.angular.template.security.dao.UserDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/gui/user")
public class UserController {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserDao userDao;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;

	@ExceptionHandler
	@ResponseBody
	public ErrorResponse handle(Exception e, HttpServletResponse response) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(e.toString() + " " + e.getMessage());
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		return errorResponse;
	}

	//http://localhost:8080/proskar-angular-template/rest/gui/user
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public UserTransfer getUser() throws NotAuthorizedException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
			throw new NotAuthorizedException();
		}
		UserDetails userDetails = (UserDetails) principal;
		UserTransfer response = new UserTransfer(userDetails.getUsername(), this.createRoleMap(userDetails));
		return response;
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	@ResponseBody
	public TokenTransfer authenticate(HttpServletRequest request, @RequestBody AuthDTO authDTO) {
		request.getSession();
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authDTO.getUsername(), authDTO.getPassword());
		Authentication authentication = this.authManager.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetails userDetails = this.userService.loadUserByUsername(authDTO.getUsername());
		return new TokenTransfer(TokenUtils.createToken(userDetails));
	}

	private Map<String, Boolean> createRoleMap(UserDetails userDetails) {
		Map<String, Boolean> roles = new HashMap<String, Boolean>();
		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			roles.put(authority.getAuthority(), Boolean.TRUE);
		}

		return roles;
	}

	//http://localhost:8080/proskar-angular-template/rest/gui/user/logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public void logout(HttpServletRequest request, HttpServletResponse response) throws NotAuthorizedException {
		SecurityContextHolder.getContext().setAuthentication(null);
		request.getSession().invalidate();
		response.setStatus(HttpServletResponse.SC_OK);
	}
}
