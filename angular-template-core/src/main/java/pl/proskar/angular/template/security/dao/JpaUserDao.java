package pl.proskar.angular.template.security.dao;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("userDao")
public class JpaUserDao implements UserDao {

	@PersistenceContext
	private EntityManager em;

	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.findByName(username);
		if (null == user) {
			throw new UsernameNotFoundException("The user with name " + username + " was not found");
		}
		return user;
	}

	@Transactional(readOnly = true)
	public User findByName(String name) {
		return (User)em.createQuery("select u from User u where u.name=:name").setParameter("name", name).getSingleResult();
	}

	@Transactional
	public void addUser(User user) {
		em.persist(user);
	}

	public List<User> findAllUsers() {
		List<User> result = em.createQuery("select c from User c order by id").getResultList();
		return result;
	}

	public User getUser(int userId) {
		return em.find(User.class, userId);
	}

	@Transactional
	public void deleteUser(int userId) {
		User user = getUser(userId);
		em.remove(user);
	}

	@Transactional
	public void updateUser(User user) {
		User userToUpdate = getUser(user.getId());
		userToUpdate.setName(user.getName());
		userToUpdate.setPassword(user.getPassword());
	}

	public List<Role> getAllRolesByUserId(int userId) {
		List<Role> result = em.createQuery("select c from Role c where c.user.id=:userId order by id").setParameter("userId", userId).getResultList();
		return result;
	}

	public Role getUserRole(int userRoleId) {
		return em.find(Role.class, userRoleId);
	}

	@Transactional
	public void addUserRole(Role role) {
		em.persist(role);
	}

	@Transactional
	public void deleteUserRole(int userRoleId) {
		Role role = getUserRole(userRoleId);
		em.remove(role);
	}

}
