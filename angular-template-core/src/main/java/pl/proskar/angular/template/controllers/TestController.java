package pl.proskar.angular.template.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping()
public class TestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);
    @Autowired
    StandardPasswordEncoder encoder;

    public TestController() {
        LOGGER.error("TestController created....");
    }

    //http://localhost:8080/proskar-angular-template/rest/gui/test
    @RequestMapping(value = "/gui/test", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getLocations() throws MalformedURLException {
        LOGGER.error("Test controlleer invoked");
        List<String> result = new ArrayList<String>();
        result.add("Test 1");
        result.add("trzech");
        System.out.println("dupa");
        result.add("czesci");
        String test = encoder.encode("admin");
        result.add(test);
        return result;
    }

    @RequestMapping(value = "/gui/test", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getLocationsAasdafsd() throws MalformedURLException {
        LOGGER.error("Test controlleer invoked");
        List<String> result = new ArrayList<String>();
        result.add("Test 1");
        result.add("trzech");
        System.out.println("dupa");
        result.add("czesci");
        String test = encoder.encode("admin");
        result.add(test);
        return result;
    }

    @RequestMapping(value = "/gui/test", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getLocationsAasdafsdASdeafs() throws MalformedURLException {
        LOGGER.error("Test controlleer invoked");
        List<String> result = new ArrayList<String>();
        result.add("Test 1");
        result.add("trzech");
        System.out.println("dupa");
        result.add("czesci");
        String test = encoder.encode("admin");
        result.add(test);
        return result;
    }
}
