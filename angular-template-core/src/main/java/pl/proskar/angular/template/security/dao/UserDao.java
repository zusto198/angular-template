package pl.proskar.angular.template.security.dao;

import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserDao extends UserDetailsService {

	User findByName(String name);

	void addUser(User user);
	List<User> findAllUsers();
	User getUser(int userId);
	void deleteUser(int userId);
	void updateUser(User user);

	List<Role> getAllRolesByUserId(int userId);
	Role getUserRole(int userRoleId);
	void addUserRole(Role role);
	void deleteUserRole(int userRoleId);

}
