package pl.proskar.angular.template.security.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testApplicationContext.xml")
public class JpaUserDaoTestIT {

    private final static Logger LOGGER = LoggerFactory.getLogger(JpaUserDaoTestIT.class);

    @Autowired
    UserDao userDao;

    @Test
    public void testName() throws Exception {
        List<User> allUsers = userDao.findAllUsers();
        for (User allUser : allUsers) {
            LOGGER.debug(allUser.getName());
        }
    }
}